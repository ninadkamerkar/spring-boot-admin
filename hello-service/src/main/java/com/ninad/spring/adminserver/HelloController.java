package com.ninad.spring.adminserver;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@RequestMapping("/hello/{name}")
	public String greeting(@PathVariable("name") String name) {
		
		StringBuilder message = new StringBuilder();
		message.append("HELLO " + name.toUpperCase());
		return message.toString();
	}
}